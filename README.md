# Advanced Sample Hardhat Project

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
npx hardhat help
REPORT_GAS=true npx hardhat test
npx hardhat coverage
npx hardhat run scripts/deploy.ts
TS_NODE_FILES=true npx ts-node scripts/deploy.ts
npx eslint '**/*.{js,ts}'
npx eslint '**/*.{js,ts}' --fix
npx prettier '**/*.{json,sol,md}' --check
npx prettier '**/*.{json,sol,md}' --write
npx solhint 'contracts/**/*.sol'
npx solhint 'contracts/**/*.sol' --fix
```

Awesome Coin which used as reward token is here [0x155359dCe4b0D7125B04E8cD8d56E31d6D0DC84C](https://rinkeby.etherscan.io/token/0x155359dCe4b0D7125B04E8cD8d56E31d6D0DC84C)

Uniswap V2 (UNI-V2) which used as stakingToken is here [0xd1dc2f7f09FE5E55C5bC828F876bcaBb4B365963](https://rinkeby.etherscan.io/address/0xd1dc2f7f09FE5E55C5bC828F876bcaBb4B365963) 

Staking contract is here [0x8Ed265DC0c9aA9594c02dc3Ea17EBBb800E60fEF](https://rinkeby.etherscan.io/address/0x8Ed265DC0c9aA9594c02dc3Ea17EBBb800E60fEF)
