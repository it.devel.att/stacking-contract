import { ethers } from "hardhat";
import { utils } from "ethers";

async function main() {
  const initialTokenBalance = utils.parseUnits("100000000", 18);
  const Stake = await ethers.getContractFactory("Token");
  const stakeToken = await Stake.deploy("Stake Token", "STK", initialTokenBalance);
  const Reward = await ethers.getContractFactory("Token");
  const rewardToken = await Reward.deploy("Reward Token", "RWD", initialTokenBalance);


  await stakeToken.deployed();
  await rewardToken.deployed();

  console.log("StakeToken deployed to:", stakeToken.address);
  console.log("RewardToken deployed to:", rewardToken.address);

  const Staking = await ethers.getContractFactory("Staking");
  const staking = await Staking.deploy(stakeToken.address, rewardToken.address);

  await staking.deployed();

  console.log("Staking contract deployed to:", staking.address);
  await rewardToken.transfer(
    staking.address,
    initialTokenBalance
  );
  console.log("Transfer all reward tokens to staking contract");
  await stakeToken.approve(staking.address, utils.parseUnits("100000", 18));
  console.log("Approve 100_000 stake coins for staking contract");
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
