import { ethers } from "hardhat";

async function main() {
  const stakeTokenAddress = process.env.STAKE_TOKEN_ADDRESS;
  const rewardTokenAddress = process.env.REWARD_TOKEN_ADDRESS;
  if (!stakeTokenAddress) {
    throw "Empty stake token address. set env varibale STAKE_TOKEN_ADDRESS";
  }
  if (!rewardTokenAddress) {
    throw "Empty rewatd token address. set env varibale REWARD_TOKEN_ADDRESS";
  }

  const Staking = await ethers.getContractFactory("Staking");
  const staking = await Staking.deploy(stakeTokenAddress, rewardTokenAddress);

  await staking.deployed();

  console.log("Staking deployed to:", staking.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
