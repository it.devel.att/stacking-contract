import { expect } from "chai";
import { ethers, network } from "hardhat";
import { Contract, utils, BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("Stacking", function () {
  let stakingToken: Contract;
  let rewardToken: Contract;
  let stakingContract: Contract;

  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addrs: SignerWithAddress[];
  let clean: any;

  const initialTokenBalance: BigNumber = utils.parseUnits("100000000", 18);
  const defaultFreezeTime: number = 1200;
  const defaultRewardSeconds: number = 600;
  const defaultRewardPercent: number = 20;

  before(async () => {
    const TokenA = await ethers.getContractFactory("Token");
    stakingToken = await TokenA.deploy(
      "StakingToken",
      "STK",
      initialTokenBalance
    );

    const TokenB = await ethers.getContractFactory("Token");
    rewardToken = await TokenB.deploy(
      "RewardToken",
      "REW",
      initialTokenBalance
    );

    const StackingContract = await ethers.getContractFactory("Staking");
    [owner, addr1, ...addrs] = await ethers.getSigners();
    stakingContract = await StackingContract.deploy(
      stakingToken.address,
      rewardToken.address
    );
    await rewardToken.transfer(
      stakingContract.address,
      utils.parseUnits("50000000", 18)
    );

    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean],
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  async function increaseTime(addSeconds: number) {
    await network.provider.request({
      method: "evm_increaseTime",
      params: [addSeconds],
    });
    await network.provider.request({
      method: "evm_mine",
      params: [],
    });
  }

  async function approveAndStake(
    account: SignerWithAddress,
    stakingToken: Contract,
    stakingContract: Contract,
    tokens: BigNumber
  ) {
    const accountBalance = await stakingToken.balanceOf(account.address);
    const stakeContractBalance = await stakingToken.balanceOf(stakingContract.address);

    await expect(stakingToken.approve(stakingContract.address, tokens)).to.be.not.reverted;
    await expect(stakingContract.stake(tokens)).to.be.not.reverted;
    const stakingContractTokenBalance = await stakingToken.balanceOf(stakingContract.address);
    const newAccountBalance = await stakingToken.balanceOf(owner.address);

    expect(stakingContractTokenBalance.toString()).to.eq(stakeContractBalance.add(tokens).toString());
    expect(newAccountBalance.toString()).to.eq(accountBalance.sub(tokens));
  }

  async function assertStakerValues(
    account: SignerWithAddress,
    stakeBalance: BigNumber,
    rewardBalance: BigNumber,
    stakeTimestamp: BigNumber,
    rewardTimestamp: BigNumber
  ) {
    const accountStaker = await stakingContract.stakers(account.address);
    expect(accountStaker.stakeBalance.toString()).to.eq(stakeBalance.toString());
    expect(accountStaker.rewardBalance.toString()).to.eq(rewardBalance.toString());
    expect(accountStaker.lastStakeTimestamp.toString()).to.eq(stakeTimestamp.toString());
    expect(accountStaker.lastRewardUpdateTimestamp.toString()).to.eq(rewardTimestamp.toString());
  }

  async function latestBlockTimestamp(): Promise<BigNumber> {
    const latestBlock = await ethers.provider.getBlock("latest");
    return utils.parseUnits(latestBlock.timestamp.toString(), 0);
  }

  function calculatePercent(value: BigNumber, percent: number): BigNumber {
    return value.div(utils.parseUnits("100", 0)).mul(utils.parseUnits(percent.toString(), 0))
  }

  it("Only admin can configurate config values", async function () {
    const value = utils.parseUnits("50", 0);

    expect((await stakingContract.getPercent()).toString()).to.not.equal(value.toString());
    await stakingContract.updatePercent(value);
    expect((await stakingContract.getPercent()).toString()).to.equal(value.toString());

    expect((await stakingContract.getRewardSeconds()).toString()).to.not.equal(value.toString());
    await stakingContract.updateRewardSeconds(value);
    expect((await stakingContract.getRewardSeconds()).toString()).to.equal(value.toString());

    expect((await stakingContract.getFreezeStakeSeconds()).toString()).to.not.equal(value.toString());
    await stakingContract.updateFreezeStakeSeconds(value);
    expect((await stakingContract.getFreezeStakeSeconds()).toString()).to.equal(value.toString());

    await stakingContract.updateFreezeStakeSeconds(value);

    await expect(stakingContract.connect(addr1).updatePercent(value)).to.be.revertedWith("Only admin action");
    await expect(stakingContract.connect(addr1).updateRewardSeconds(value)).to.be.revertedWith("Only admin action");
    await expect(stakingContract.connect(addr1).updateFreezeStakeSeconds(value)).to.be.revertedWith("Only admin action");
  });

  it("Should stake 5000 coins from StakeToken to StakeContract", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await expect(stakingToken.approve(stakingContract.address, tokens)).to.be.not.reverted;
    await expect(stakingContract.stake(tokens)).to.be.not.reverted;
    const stakingContractTokenBalance = await stakingToken.balanceOf(stakingContract.address);
    const ownerStakeTokenBalance = await stakingToken.balanceOf(owner.address);

    expect(stakingContractTokenBalance.toString()).to.eq(tokens.toString());
    expect(ownerStakeTokenBalance.toString()).to.eq(initialTokenBalance.sub(tokens));
  });

  it("Stake schould update staker values", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    const lastBlockTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      lastBlockTimestamp,
      lastBlockTimestamp
    );
  });

  it("Double stake schould increase stake balance", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    let lastBlockTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      lastBlockTimestamp,
      lastBlockTimestamp
    );
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    lastBlockTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens.add(tokens),
      utils.parseUnits("0"),
      lastBlockTimestamp,
      lastBlockTimestamp
    );
  });

  it("Calculate reward on second stake pass time for 1 reward", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    let lastBlockTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      lastBlockTimestamp,
      lastBlockTimestamp
    );

    await increaseTime(defaultRewardSeconds);

    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    lastBlockTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens.add(tokens),
      calculatePercent(tokens, defaultRewardPercent),
      lastBlockTimestamp,
      lastBlockTimestamp
    );
  });

  it("Cannot unstake immidiatly after stake", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    await expect(stakingContract.unstake()).to.be.revertedWith("Freeze time don't pass");
  });

  it("Cannot unstake if never stake", async function () {
    await expect(stakingContract.unstake()).to.be.revertedWith("Never stake");
  });

  it("Success unstake after freezetime and return stakeToken balances", async function () {
    const tokens = utils.parseUnits("5000", 18);
    const accountInitialBalance = await stakingToken.balanceOf(owner.address);
    const stakingContractInitBalance = await stakingToken.balanceOf(stakingContract.address);

    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      await latestBlockTimestamp(),
      await latestBlockTimestamp()
    );
    await increaseTime(defaultFreezeTime);
    await expect(stakingContract.unstake()).to.be.not.reverted;

    const newAccountBalance = await stakingToken.balanceOf(owner.address);
    const newStakingContractBalance = await stakingToken.balanceOf(stakingContract.address);
    await expect(newAccountBalance.toString()).to.eq(accountInitialBalance.toString());
    await expect(newStakingContractBalance.toString()).to.eq(stakingContractInitBalance.toString());
  });

  it("Don't calculate reward on unstake", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    const stakeLatestBlockTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      stakeLatestBlockTimestamp,
      stakeLatestBlockTimestamp
    );
    await increaseTime(defaultFreezeTime);

    await expect(stakingContract.unstake()).to.be.not.reverted;
    await assertStakerValues(
      owner,
      tokens.sub(tokens),
      utils.parseUnits("0"),
      stakeLatestBlockTimestamp,
      stakeLatestBlockTimestamp
    );
  });

  it("Cannot unstake zero balance", async function () {
    const tokens = utils.parseUnits("5000", 18);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      await latestBlockTimestamp(),
      await latestBlockTimestamp()
    );
    await increaseTime(defaultFreezeTime);
    await expect(stakingContract.unstake()).to.be.not.reverted;
    await expect(stakingContract.unstake()).to.be.revertedWith("Zero balance");
  });

  it("Claim reward from rewardToken", async function () {
    const tokens = utils.parseUnits("5000", 18);
    const existAccountRewardBalance = await rewardToken.balanceOf(owner.address);
    const existStakingRewardBalance = await rewardToken.balanceOf(stakingContract.address);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    const stakeTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      stakeTimestamp,
      stakeTimestamp
    );

    await increaseTime(defaultRewardSeconds);
    await stakingContract.claim();
    const newAccountRewardBalance = await rewardToken.balanceOf(owner.address);
    const newStakingRewardBalance = await rewardToken.balanceOf(stakingContract.address);

    expect(newAccountRewardBalance.toString()).to.eq(existAccountRewardBalance.add(utils.parseUnits("1000", 18)));
    expect(newStakingRewardBalance.toString()).to.eq(existStakingRewardBalance.sub(utils.parseUnits("1000", 18)));
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      stakeTimestamp,
      await latestBlockTimestamp()
    );
  });

  it("Claim few times with reward seconds waits", async function () {
    const tokens = utils.parseUnits("5000", 18);
    const existAccountRewardBalance = await rewardToken.balanceOf(owner.address);
    const existStakingRewardBalance = await rewardToken.balanceOf(stakingContract.address);
    await approveAndStake(owner, stakingToken, stakingContract, tokens);
    const stakeTimestamp = await latestBlockTimestamp();
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      stakeTimestamp,
      stakeTimestamp
    );

    await increaseTime(defaultRewardSeconds);
    await stakingContract.claim();
    await increaseTime(defaultRewardSeconds);
    await stakingContract.claim();

    const newAccountRewardBalance = await rewardToken.balanceOf(owner.address);
    const newStakingRewardBalance = await rewardToken.balanceOf(stakingContract.address);

    expect(newAccountRewardBalance.toString()).to.eq(existAccountRewardBalance.add(utils.parseUnits("2000", 18)));
    expect(newStakingRewardBalance.toString()).to.eq(existStakingRewardBalance.sub(utils.parseUnits("2000", 18)));
    await assertStakerValues(
      owner,
      tokens,
      utils.parseUnits("0"),
      stakeTimestamp,
      await latestBlockTimestamp()
    );
  });
});
