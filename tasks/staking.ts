import { task } from "hardhat/config";
import { Staking } from "../typechain/Staking.d";

async function getContract(hre: any, contractAddress: string): Promise<Staking> {
  const Token = await hre.ethers.getContractFactory("Staking");
  const contract = await Token.attach(contractAddress);
  return contract
}

task("stake", "Stake stakeCoins to contract")
  .addParam("contract", "Address of Staking contract")
  .addParam("amount", "Amount of tokens for stake")
  .setAction(async (taskArgs, hre) => {
    const contract = await getContract(hre, taskArgs.contract);
    await contract.stake(taskArgs.amount);
    console.log(`Success stake ${taskArgs.amount} coins`);
  });

task("unstake", "Unstake return all stake coins")
  .addParam("contract", "Address of Staking contract")
  .setAction(async (taskArgs, hre) => {
    const contract = await getContract(hre, taskArgs.contract);
    await contract.unstake();
    console.log(`Success return stake coins`);
  });

task("claim", "Claim calculate reward tokens and transfer to account")
  .addParam("contract", "Address of Staking contract")
  .setAction(async (taskArgs, hre) => {
    const contract = await getContract(hre, taskArgs.contract);
    await contract.claim();
    console.log(`Success claim coins`);
  });
